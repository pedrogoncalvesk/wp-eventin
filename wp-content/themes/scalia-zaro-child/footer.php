<?php
/**
 * The template for displaying the footer
 */
?>

</div><!-- #main -->



<?php if(is_active_sidebar('footer-widget-area')) : ?>
    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="container">
            <?php get_sidebar('footer'); ?>
            <div class="row"><div class="col-md-4 col-xs-12"></div></div>
        </div>
    </footer><!-- #colophon -->
<?php endif; ?>
<footer id="footer-nav" class="site-footer">
    <div class="container"><div class="row">
            <div class="col-md-4 col-xs-12">
                <?php
                $socials_icons = array('twitter' => scalia_get_option('twitter_active'), 'facebook' => scalia_get_option('facebook_active'), 'linkedin' => scalia_get_option('linkedin_active'), 'googleplus' => scalia_get_option('googleplus_active'), 'stumbleupon' => scalia_get_option('stumbleupon_active'), 'rss' => scalia_get_option('rss_active'));
                if(in_array(1, $socials_icons)) : ?>
                    <div id="footer-socials" class="socials">
                        <?php foreach($socials_icons as $name => $active) : ?>
                            <?php if($active) : ?>
                                <div  class="footer-socials-item <?php echo esc_attr($name); ?>"><a href="<?php echo esc_url(scalia_get_option($name . '_link')); ?>" target="_blank" title="<?php echo esc_attr($name); ?>"><?php echo $name; ?></a></div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div><!-- #footer-socials -->
                <?php endif; ?>
            </div>
            <?php if(scalia_get_option('footer_html')) : ?>

            <div class="col-md-8 col-xs-12">
                <div class="footer-site-info"><?php echo do_shortcode(nl2br(strip_tags(stripslashes(scalia_get_option('footer_html'))))); ?></div>

            </div>
            <?php endif; ?>
        </div>
    </div>
</footer><!-- #footer-nav -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>