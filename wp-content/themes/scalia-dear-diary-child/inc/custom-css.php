<?php if(scalia_get_option('box_border_color')) : ?>
.sc-button,
input[type='submit'],
#primary-menu.no-responsive > li ul,
#colophon .sc-custom-mailchimp input[type="email"] {
	border: 1px solid <?php echo scalia_get_option('box_border_color'); ?>;
}
.blog-load-more .sc-button-separator,
.blog-load-more .sc-button-separator,
.portfolio-load-more .sc-button-separator,
.portfolio-load-more .sc-button-separator,
#primary-menu.no-responsive {
	border-top: 1px solid <?php echo scalia_get_option('box_border_color'); ?>;
	border-bottom: 1px solid <?php echo scalia_get_option('box_border_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('main_background_color')) : ?>
.sc-gallery-hover-zooming-blur .sc-gallery-preview-carousel-wrap .sc-gallery-item a:after,
.sc-image a:after,
.hover-zooming-blur .sc-gallery-item .overlay-content a.icon:before,
.portfolio.hover-zooming-blur .portfolio-item .image .overlay .links a.icon:before,
.sc-gallery-grid.hover-zooming-blur .gallery-item .overlay a.icon:before,
.blog.blog-style-default article a.default:after,
div.blog article a.default:after,
.blog-style-default article .post-image .sc-dummy:after {
	background-color: <?php echo scalia_get_option('main_background_color'); ?>;
}
.footer-socials-item a:after,
.footer-socials-item a:hover:after,
#main .blog-style-default .blog-post-date-day {
	color: <?php echo scalia_get_option('main_background_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('icons_slideshow_hovers_color')) : ?>
.sc-gallery .sc-gallery-preview-carousel-wrap .sc-gallery-item a:after {
	color: <?php echo scalia_get_option('icons_slideshow_hovers_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('footer_social_color')) : ?>
.footer-socials-item a,
#main .blog-style-default .blog-post-date {
	background-color: <?php echo scalia_get_option('footer_social_color'); ?>;
}
#colophon {
	border-bottom: 15px solid <?php echo scalia_get_option('footer_social_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('portfolio_title_color')) : ?>
.sc-gallery-hover-zooming-blur .sc-gallery-preview-carousel-wrap .sc-gallery-item .sc-gallery-caption,
.portfolio.hover-zooming-blur .portfolio-item .image .overlay .links .caption .title,
.sc-gallery-grid.hover-zooming-blur .gallery-item .overlay .title,
.portfolio.hover-zooming-blur .portfolio-item .image .overlay .links .caption .subtitle,
.portfolio.hover-zooming-blur .portfolio-item .image .overlay .links .caption .info,
.sc-gallery-grid.hover-zooming-blur .gallery-item .overlay .subtitle {
	color: <?php echo scalia_get_option('portfolio_title_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('footer_social_hover_color')) : ?>
.footer-socials-item a:hover {
	background-color: <?php echo scalia_get_option('footer_social_hover_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('h4_font_family')) : ?>
.footer-widget-area .widget-title,
.blog-style-timeline .post-time {
	font-family: '<?php echo scalia_get_option('h4_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('h4_font_style')) : ?>
.footer-widget-area .widget-title,
.blog-style-timeline .post-time {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('h4_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('h4_font_style'), 'italic') !== false) : ?>
.footer-widget-area .widget-title,
.blog-style-timeline .post-time {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('h4_font_size')) : ?>
.footer-widget-area .widget-title,
.blog-style-timeline .post-time {
	font-size: <?php echo scalia_get_option('h4_font_size'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('h4_line_height')) : ?>
.footer-widget-area .widget-title {
	line-height: <?php echo scalia_get_option('h4_line_height'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('h1_color')) : ?>
.sc-testimonial-text {
	color: <?php echo scalia_get_option('h1_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('h4_color')) : ?>
#main .blog-style-timeline .post-time span {
	color: <?php echo scalia_get_option('h4_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('button_line_height')) : ?>
.blog-style-default .entry-info,
.blog.blog-style-masonry article .description .info,
.blog.blog-style-timeline .styled-blog-meta,
#colophon .sc-custom-mailchimp input[type="email"] {
	line-height: <?php echo scalia_get_option('button_line_height'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('h3_font_family')) : ?>
#main .blog-style-default .blog-post-date-day {
	font-family: '<?php echo scalia_get_option('h3_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('h3_font_style')) : ?>
#main .blog-style-default .blog-post-date-day {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('h3_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('h3_font_style'), 'italic') !== false) : ?>
#main .blog-style-default .blog-post-date-day {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('h3_font_size')) : ?>
#main .blog-style-default .blog-post-date-day {
	font-size: <?php echo scalia_get_option('h3_font_size'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('styled_elements_background_color')) : ?>
.blog-style-default article .post-image .sc-dummy,
.blog-style-default article.no-image .post-image .sc-dummy,
.testimonials-style-1-block .sc-testimonials-carousel-wrap,
.triangle-with-shadow:after {
	background-color: <?php echo scalia_get_option('styled_elements_background_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('divider_default_color')) : ?>
#main .blog-style-default article + article,
#main .blog-style-default article.sticky + article:before,
#main .blog-style-default .comments-link:after,
#main .blog-style-default article.sticky .comments-link:before,
.sc-testimonial-image:before,
.sc-testimonial-image:after {
	border-top: 1px solid <?php echo scalia_get_option('divider_default_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('link_color')) : ?>
#main .blog-style-default .comments-link a,
.blog article .entry-title a:hover,
.portfolio-item .caption .title,
.blog.blog-style-masonry article .description .title,
.blog.blog-style-masonry article .description .title a:hover {
	color: <?php echo scalia_get_option('link_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('hover_link_color')) : ?>
#main .blog-style-default .comments-link a:hover,
.blog article .entry-title a {
	color: <?php echo scalia_get_option('hover_link_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('active_link_color')) : ?>
#main .blog-style-default .comments-link a:active {
	color: <?php echo scalia_get_option('active_link_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('form_elements_background_color')) : ?>
#colophon .sc-custom-mailchimp input[type="email"] {
	background-color: <?php echo scalia_get_option('form_elements_background_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('body_color')) : ?>
.sc_accordion_header a {
	color: <?php echo scalia_get_option('body_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('footer_widget_text_color')) : ?>
#colophon .widget .sc-contacts-email:before,
#colophon .widget .sc-contacts-phone:before,
#colophon .widget .sc-contacts-address:before,
#colophon .widget .sc-contacts-fax:before,
#colophon .widget .sc-contacts-website:before {
	color: <?php echo scalia_get_option('footer_widget_text_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('h5_font_family')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.title {
	font-family: '<?php echo scalia_get_option('h5_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('h5_font_style')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.title {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('h5_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('h5_font_style'), 'italic') !== false) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.title {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('h5_color')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.title,
.diagram-circle .text div span.summary{
	color: <?php echo scalia_get_option('h5_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('body_font_family')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.summary {
	font-family: '<?php echo scalia_get_option('body_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('body_font_style')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.summary {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('body_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('body_font_style'), 'italic') !== false) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.summary {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('body_color')) : ?>
.diagram-circle .text div,
.diagram-circle .text div span.summary {
	color: <?php echo scalia_get_option('body_color'); ?>;
}
<?php endif; ?>
