<?php if(scalia_get_option('main_menu_font_family')) : ?>
#footer-menu > li > a {
	font-family: '<?php echo scalia_get_option('main_menu_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_font_style')) : ?>
#footer-menu > li > a {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('main_menu_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('main_menu_font_style'), 'italic') !== false) : ?>
#footer-menu > li > a {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('main_menu_font_size')) : ?>
#footer-menu > li > a {
	font-size: <?php echo scalia_get_option('main_menu_font_size'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_line_height')) : ?>
#footer-menu > li > a {
	line-height: <?php echo scalia_get_option('main_menu_line_height'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_text_color')) : ?>
#footer-menu > li > a {
	color: <?php echo scalia_get_option('main_menu_text_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_hover_text_color')) : ?>
#footer-menu > li:hover > a,
#footer-menu > li li.menu-item-parent > a:after {
	color: <?php echo scalia_get_option('main_menu_hover_text_color'); ?>;
}
#footer-menu > li > ul > li> ul a:hover,
#footer-menu > li > ul > li> ul li.menu-item-active > a,
#footer-menu > li > ul > li> ul li.menu-item-current > a {
	background-color: <?php echo scalia_get_option('main_menu_hover_text_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_active_text_color')) : ?>
#footer-menu > li.menu-item-current > a,
#footer-menu > li.menu-item-active > a {
	color: <?php echo scalia_get_option('main_menu_active_text_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('main_menu_active_background_color')) : ?>
#footer-menu > li.menu-item-current > a,
#footer-menu > li.menu-item-active > a {
	background-color: <?php echo scalia_get_option('main_menu_active_background_color'); ?>;
}
<?php endif; ?>

<?php if(scalia_get_option('h6_font_family')) : ?>
.project_info-item-style-2 .title {
	font-family: '<?php echo scalia_get_option('h6_font_family'); ?>';
}
<?php endif; ?>

<?php if(scalia_get_option('h6_font_style')) : ?>
.project_info-item-style-2 .title {
	font-weight: <?php echo str_replace(array('italic', 'regular'),array('', 'normal'),scalia_get_option('h6_font_style')); ?>;
}
<?php if(strpos(scalia_get_option('h6_font_style'), 'italic') !== false) : ?>
.project_info-item-style-2 .title {
	font-style: italic;
}
<?php endif; ?>
<?php endif; ?>

<?php if(scalia_get_option('h6_font_size')) : ?>
.project_info-item-style-2 .title {
	font-size: <?php echo scalia_get_option('h6_font_size'); ?>px;
}
<?php endif; ?>

<?php if(scalia_get_option('h6_line_height')) : ?>
.project_info-item-style-2 .title {
	line-height: <?php echo scalia_get_option('h6_line_height'); ?>px;
}
<?php endif; ?>