<?php

add_action('wp_enqueue_scripts', 'enqueue_parent_theme_style');
function enqueue_parent_theme_style() {
	wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css', array('scalia-icons', 'scalia-reset', 'scalia-grid'));
	wp_enqueue_script('scalia-smoothscroll', get_stylesheet_directory_uri() . '/js/jquery.simplr.smoothscroll.min.js', array('jquery'));
	wp_enqueue_script('scalia-respond', get_stylesheet_directory_uri() . '/js/functions.js', array('jquery', 'scalia-smoothscroll'));
}

function scalia_scrollex_default_skins_options($array = array()) {
	return array(
		'light' => array(
			'main_menu_font_family' => 'Montserrat',
			'main_menu_font_style' => 'regular',
			'main_menu_font_sets' => '',
			'main_menu_font_size' => '16',
			'main_menu_line_height' => '20',
			'submenu_font_family' => 'Montserrat',
			'submenu_font_style' => 'regular',
			'submenu_font_sets' => '',
			'submenu_font_size' => '16',
			'submenu_line_height' => '30',
			'styled_subtitle_font_family' => 'Dosis',
			'styled_subtitle_font_style' => '200',
			'styled_subtitle_font_sets' => '',
			'styled_subtitle_font_size' => '48',
			'styled_subtitle_line_height' => '57',
			'blog_post_title_font_family' => 'Dosis',
			'blog_post_title_font_style' => '200',
			'blog_post_title_font_sets' => '',
			'blog_post_title_font_size' => '48',
			'blog_post_title_line_height' => '57',
			'h1_font_family' => 'Montserrat',
			'h1_font_style' => '700',
			'h1_font_sets' => '',
			'h1_font_size' => '100',
			'h1_line_height' => '134',
			'h2_font_family' => 'Montserrat',
			'h2_font_style' => '700',
			'h2_font_sets' => '',
			'h2_font_size' => '70',
			'h2_line_height' => '134',
			'h3_font_family' => 'Montserrat',
			'h3_font_style' => '700',
			'h3_font_sets' => '',
			'h3_font_size' => '50',
			'h3_line_height' => '80',
			'h4_font_family' => 'Montserrat',
			'h4_font_style' => '700',
			'h4_font_sets' => '',
			'h4_font_size' => '40',
			'h4_line_height' => '41',
			'h5_font_family' => 'Montserrat',
			'h5_font_style' => '700',
			'h5_font_sets' => '',
			'h5_font_size' => '30',
			'h5_line_height' => '34',
			'h6_font_family' => 'Montserrat',
			'h6_font_style' => '700',
			'h6_font_sets' => '',
			'h6_font_size' => '20',
			'h6_line_height' => '28',
			'body_font_family' => 'Dosis',
			'body_font_style' => '300',
			'body_font_sets' => '',
			'body_font_size' => '21',
			'body_line_height' => '27',
			'widget_title_font_family' => 'Montserrat',
			'widget_title_font_style' => '700',
			'widget_title_font_sets' => '',
			'widget_title_font_size' => '30',
			'widget_title_line_height' => '40',
			'button_font_family' => 'Montserrat',
			'button_font_style' => 'regular',
			'button_font_sets' => '',
			'button_font_size' => '30',
			'button_line_height' => '52',
			'slideshow_title_font_family' => 'Roboto Condensed',
			'slideshow_title_font_style' => '300',
			'slideshow_title_font_sets' => '',
			'slideshow_title_font_size' => '80',
			'slideshow_title_line_height' => '100',
			'slideshow_description_font_family' => 'Roboto',
			'slideshow_description_font_style' => '100',
			'slideshow_description_font_sets' => '',
			'slideshow_description_font_size' => '36',
			'slideshow_description_line_height' => '50',
			'portfolio_title_font_family' => 'Dosis',
			'portfolio_title_font_style' => '300',
			'portfolio_title_font_sets' => '',
			'portfolio_title_font_size' => '21',
			'portfolio_title_line_height' => '30',
			'portfolio_description_font_family' => 'Dosis',
			'portfolio_description_font_style' => '300',
			'portfolio_description_font_sets' => '',
			'portfolio_description_font_size' => '19',
			'portfolio_description_line_height' => '27',
			'quickfinder_title_font_family' => 'Roboto Condensed',
			'quickfinder_title_font_style' => 'regular',
			'quickfinder_title_font_sets' => '',
			'quickfinder_title_font_size' => '21',
			'quickfinder_title_line_height' => '30',
			'quickfinder_description_font_family' => 'Source Sans Pro',
			'quickfinder_description_font_style' => '300',
			'quickfinder_description_font_sets' => '',
			'quickfinder_description_font_size' => '17',
			'quickfinder_description_line_height' => '24',
			'gallery_title_font_family' => 'Roboto Condensed',
			'gallery_title_font_style' => 'regular',
			'gallery_title_font_sets' => '',
			'gallery_title_font_size' => '21',
			'gallery_title_line_height' => '30',
			'gallery_description_font_family' => 'Source Sans Pro',
			'gallery_description_font_style' => '300',
			'gallery_description_font_sets' => '',
			'gallery_description_font_size' => '17',
			'gallery_description_line_height' => '24',
			'pricing_table_price_font_family' => 'Montserrat',
			'pricing_table_price_font_style' => '700',
			'pricing_table_price_font_sets' => '',
			'pricing_table_price_font_size' => '50',
			'pricing_table_price_line_height' => '56',
			'testimonial_font_family' => 'Montserrat',
			'testimonial_font_style' => '700',
			'testimonial_font_sets' => '',
			'testimonial_font_size' => '30',
			'testimonial_line_height' => '90',
			'counter_font_family' => 'Roboto',
			'counter_font_style' => '300',
			'counter_font_sets' => '',
			'counter_font_size' => '59',
			'counter_line_height' => '66',
			'woocommerce_price_font_family' => 'Roboto',
			'woocommerce_price_font_style' => '300',
			'woocommerce_price_font_sets' => '',
			'woocommerce_price_font_size' => '26',
			'woocommerce_price_line_height' => '36',
			'highlight_label_font_family' => 'Dosis',
			'highlight_label_font_style' => '300',
			'highlight_label_font_sets' => '',
			'highlight_label_font_size' => '21',
			'highlight_label_line_height' => '18',
			'basic_outer_background_color' => '#8b94a5',
			'basic_inner_background_color' => '#e8ecef',
			'top_background_color' => '#242c33',
			'main_background_color' => '#ffffff',
			'footer_background_color' => '',
			'styled_elements_background_color' => '#f1f5f8',
			'styled_elements_color_1' => '#ff7b61',
			'styled_elements_color_2' => '#ffb932',
			'divider_default_color' => '#d2dae1',
			'box_border_color' => '#d2dae1',
			'box_shadow_color' => '',
			'highlight_label_color' => '#ffb932',
			'main_menu_text_color' => '#ffffff',
			'main_menu_hover_text_color' => '#ffb932',
			'main_menu_active_text_color' => '#ffb932',
			'main_menu_active_background_color' => '',
			'submenu_text_color' => '#242c33',
			'submenu_hover_text_color' => '#58abb7',
			'submenu_background_color' => '#ffffff',
			'submenu_hover_background_color' => '#f1f5f8',
			'mega_menu_icons_color' => '#99a3b0',
			'menu_shadow_color' => '#4c5867',
			'body_color' => '#242c33',
			'h1_color' => '#242c33',
			'h2_color' => '#242c33',
			'h3_color' => '#242c33',
			'h4_color' => '#242c33',
			'h5_color' => '#242c33',
			'h6_color' => '#242c33',
			'link_color' => '#ff7b61',
			'hover_link_color' => '#0d2131',
			'active_link_color' => '#0d2131',
			'footer_text_color' => '#ffffff',
			'copyright_text_color' => '#ffffff',
			'copyright_link_color' => '#ffb932',
			'title_bar_background_color' => '#58ABB7',
			'title_bar_text_color' => '#ffffff',
			'top_area_text_color' => '#99a3b0',
			'date_filter_subtitle_color' => '#99a3b0',
			'system_icons_font' => '#99a3b0',
			'system_icons_font_2' => '#d2dae1',
			'button_text_basic_color' => '#242c33',
			'button_text_hover_color' => '#242c33',
			'button_text_active_color' => '#242c33',
			'button_background_basic_color' => '#ffb932',
			'button_background_hover_color' => '#ffb932',
			'button_background_active_color' => '#ffb932',
			'widget_title_color' => '',
			'widget_link_color' => '#418f9a',
			'widget_hover_link_color' => '#384554',
			'widget_active_link_color' => '#384554',
			'footer_widget_area_background_color' => '',
			'footer_widget_title_color' => '#ffb932',
			'footer_widget_text_color' => '#ffffff',
			'footer_widget_link_color' => '#58abb7',
			'footer_widget_hover_link_color' => '#ffb932',
			'footer_widget_active_link_color' => '#ffb932',
			'portfolio_slider_arrow_color' => '#58abb7',
			'portfolio_title_background_color' => '',
			'portfolio_title_color' => '#4c5867',
			'portfolio_description_color' => '#627080',
			'portfolio_sharing_button_background_color' => '#242c33',
			'portfolio_date_color' => '#99a3b0',
			'gallery_caption_background_color' => '',
			'gallery_title_color' => '',
			'gallery_description_color' => '',
			'slideshow_arrow_background' => '',
			'slideshow_arrow_color' => '#ff7070',
			'hover_effect_default_color' => '#161b21',
			'hover_effect_zooming_blur_color' => '#ffffff',
			'hover_effect_horizontal_sliding_color' => '#ff7070',
			'hover_effect_vertical_sliding_color' => '#384554',
			'quickfinder_bar_background_color' => '#ffffff',
			'quickfinder_bar_title_color' => '#4c5867',
			'quickfinder_bar_description_color' => '#384554',
			'quickfinder_title_color' => '#4c5867',
			'quickfinder_description_color' => '#384554',
			'bullets_symbol_color' => '#242c33',
			'icons_symbol_color' => '#58abb7',
			'pagination_border_color' => '',
			'pagination_text_color' => '#418f9a',
			'pagination_active_text_color' => '#ffffff',
			'pagination_active_background_color' => '#ffb932',
			'icons_slideshow_hovers_color' => '#ffffff',
			'mini_pagination_color' => '#d2dae1',
			'mini_pagination_active_color' => '#ff7070',
			'footer_social_color' => '#ffffff',
			'footer_social_hover_color' => '#ffb932',
			'top_area_icons_color' => '#99a3b0',
			'top_area_social_hover_color' => '#384554',
			'form_elements_background_color' => '#ffffff',
			'form_elements_text_color' => '#384554',
			'form_elements_border_color' => '#d2dae1',
			'basic_outer_background_image' => '',
			'basic_inner_background_image' => '',
			'top_background_image' => '',
			'main_background_image' => '',
			'footer_background_image' => '',
			'footer_widget_area_background_image' => get_stylesheet_directory_uri().'/images/footer-bg.jpg',
		)
	);
}
add_filter('scalia_default_skins_options', 'scalia_scrollex_default_skins_options');

function scalia_scrollex_first_install_settings() {
	return array(
		'page_layout_style' => 'fullwidth',
		'header_on_slideshow' => '1',
		'menu_appearance_tablet_portrait' => 'centered',
		'menu_appearance_tablet_landscape' => 'centered',
		'top_area_style' => '0',
		'top_area_search' => '1',
		'top_area_contacts' => '1',
		'top_area_socials' => '1',
		'logo' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'small_logo' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'logo_2x' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'small_logo_2x' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'logo_3x' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'small_logo_3x' => get_stylesheet_directory_uri().'/images/scrollex-logo.png',
		'logo_position' => 'center',
		'favicon' => get_stylesheet_directory_uri().'/images/favicon.ico',
		'preloader_style' => 'preloader-2',
		'custom_css' => '.slideshow-preloader {
	height: 1100px;
}
.top-area-style-2 {
	border: 0 none;
}
.about-text {
	position: relative;
	z-index: 6;
}
.gear-1,
.gear-2,
.gear-3,
.gear-4,
.gear-5 {
	position: absolute;
	margin: 0;
}
.gear-4 {
	z-index: 5;
}
.gear-5 {
	z-index: 4;
}
.gear-1 {
	z-index: 3;
}
.gear-3 {
	z-index: 2;
}
.gear-2 {
	z-index: 1;
}
.gear-1 {
	left: 70%;
	top: 5%;
}
.gear-2 {
	left: 80%;
	top: 20%;
}
.gear-3 {
	left: 55%;
	top: 55%;
}
.gear-4 {
	left: 58%;
	top: 45%;
}
.gear-5 {
	left: 73%;
	top: 65%;
}

.clock-arrow-1,
.clock-arrow-2 {
	position: absolute;
	left: 50%;
	top: 313px;
	margin: 0;
	margin-left: -569px;
}
.design-hot {
	position: absolute;
	margin-left: -135px;
	top: 178px;
}
@keyframes rotate {
	0% { transform: rotate(0deg);}
	100% { transform: rotate(360deg);}
}
@-o-keyframes rotate {
	0% { transform: rotate(0deg);}
	100% { transform: rotate(360deg);}
}
@-moz-keyframes rotate {
	0% { transform: rotate(0deg);}
	100% { transform: rotate(360deg);}
}
@-webkit-keyframes rotate {
	0% { transform: rotate(0deg);}
	100% { transform: rotate(360deg);}
}

.vector-gear-1,
.vector-gear-2,
.vector-gear-3,
.vector-gear-4,
.vector-gear-5,
.vector-gear-6 {
	position: absolute;
	left: 50%;
	margin: 0;
	-o-animation: rotate 14s infinite linear;
	-moz-animation: rotate 14s infinite linear;
	-webkit-animation: rotate 14s infinite linear;
	animation: rotate 14s infinite linear;
}

.vector-gear-1.wpb_single_image img,
.vector-gear-2.wpb_single_image img,
.vector-gear-3.wpb_single_image img,
.vector-gear-4.wpb_single_image img,
.vector-gear-5.wpb_single_image img,
.vector-gear-6.wpb_single_image img{
	max-width: none;
}

.vector-gear-1 {
	margin-left: -60px;
	bottom: -640px;
	-o-animation-duration: 26.4s;
	-moz-animation-duration: 26.4s;
	-webkit-animation-duration: 26.4s;
	animation-duration: 26.4s;
}
.vector-gear-2 {
	margin-left: -366px;
	bottom: -290px;
	-o-animation-direction: reverse;
	-moz-animation-direction: reverse;
	-webkit-animation-direction: reverse;
	animation-direction: reverse;
	-o-animation-duration: 7s;
	-moz-animation-duration: 7s;
	-webkit-animation-duration: 7s;
	animation-duration: 7s;
}
.vector-gear-3 {
	margin-left: -630px;
	top: -365px;
	-o-animation-duration: 7s;
	-moz-animation-duration: 7s;
	-webkit-animation-duration: 7s;
	animation-duration: 7s;
}
.vector-gear-4 {
	margin-left: 136px;
	top: 398px;
	-o-animation-direction: reverse;
	-moz-animation-direction: reverse;
	-webkit-animation-direction: reverse;
	animation-direction: reverse;
	-o-animation-duration: 6s;
	-moz-animation-duration: 6s;
	-webkit-animation-duration: 6s;
	animation-duration: 6s;
}
.vector-gear-5 {
	margin-left: -130px;
	top: 65px;
	-o-animation-direction: reverse;
	-moz-animation-direction: reverse;
	-webkit-animation-direction: reverse;
	animation-direction: reverse;
	-o-animation-duration: 4s;
	-moz-animation-duration: 4s;
	-webkit-animation-duration: 4s;
	animation-duration: 4s;
}
.vector-gear-6 {
	margin-left: 455px;
	top: 400px;
	-o-animation-duration: 1.3s;
	-moz-animation-duration:1.3s;
	-webkit-animation-duration: 1.3s;
	animation-duration: 1.3s;
}
@media (max-width: 767px) {
	.vector-gear-1,
	.vector-gear-2,
	.vector-gear-3,
	.vector-gear-4,
	.vector-gear-5,
	.vector-gear-6 {
		-o-animation: none 0;
		-moz-animation: none 0;
		-webkit-animation: none 0;
		animation: none 0;
	}
	#design .fullwidth-block {
		background-image: none !important;
	}
	.clock-arrow-1,
	.clock-arrow-2,
	.design-hot,
	.gear-4 {
		display: none;
	}
}
@media (max-width: 480px) {
	.styled-subtitle {
		font-size: 38px;
		line-height: 45px;
	}
}
#call-us .sc-icon-size-big.sc-simple-icon {
	height: 108px;
	line-height: 108px;
	width: 108px;
	font-size: 108px;
}


/*   ---------------------------------- Slider Bouncing Arrow Tipp Animation --------------------------------  */ 

#Scrollex_Arrow_Tipp {
	-o-animation: scroll_tipp 5s infinite ease-in-out;
	-moz-animation: scroll_tipp 5s infinite ease-in-out;
	-webkit-animation: scroll_tipp 5s infinite ease-in-out;
	animation: scroll_tipp 5s infinite ease-in-out;
}
@keyframes scroll_tipp {
	0% { transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	40% { transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	50% { transform: rotate(0deg) scale(1); top: 92%; left: 50%; opacity: 1;}
	60% { transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	100% { transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
}
@-o-keyframes scroll_tipp {
	0% { -o-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	40% { -o-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	50% { -o-transform: rotate(0deg) scale(1); top: 92%; left: 50%; opacity: 1;}
	60% { -o-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	100% { -o-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
}
@-moz-keyframes scroll_tipp {
	0% { -moz-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	40% { -moz-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	50% { -moz-transform: rotate(0deg) scale(1); top: 92%; left: 50%; opacity: 1;}
	60% { -moz-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	100% { -moz-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
}
@-webkit-keyframes scroll_tipp {
	0% { -webkit-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	40% { -webkit-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	50% { -webkit-transform: rotate(0deg) scale(1); top: 92%; left: 50%; opacity: 1;}
	60% { -webkit-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
	100% { -webkit-transform: rotate(0deg) scale(1); top: 89%; left: 50%; opacity: 0;}
}',
		'custom_js' => '(function($) {

	$.fn.scrolling = function(speed) {
		var $item = $(this);
		var $parentBlock = $(this).closest(\'.fullwidth-block\');
		var positionTop = $item.data(\'positionTop\');
		var newPositionTop = positionTop+($(window).scrollTop()-$parentBlock.offset().top)*speed;
		$item.css({top: newPositionTop});
	}

	$.fn.arrowRotate = function(speed) {
		var $item = $(this);
		var $parentBlock = $(this).closest(\'.fullwidth-block\');
		var angel = ($(window).scrollTop()-$parentBlock.offset().top)*speed*360/$parentBlock.outerHeight();
		$item.css({
			transform: \'rotate(\'+angel+\'deg)\',
			MozTransform: \'rotate(\'+angel+\'deg)\',
			WebkitTransform: \'rotate(\'+angel+\'deg)\',
			msTransform: \'rotate(\'+angel+\'deg)\'
		})
	}

	$(function() {
		if($(\'.home-constructor\').length) {
		$(\'.gear-2\').data(\'positionTop\', $(\'.gear-2\').position().top);
		$(\'.gear-3\').data(\'positionTop\', $(\'.gear-3\').position().top);
		$(\'.gear-1\').data(\'positionTop\', $(\'.gear-1\').position().top);
		$(\'.gear-5\').data(\'positionTop\', $(\'.gear-5\').position().top);
		$(\'.gear-4\').data(\'positionTop\', $(\'.gear-4\').position().top);

		$(window).scroll(function() {
			if($(window).width() > 767) {
			$(\'.gear-4\').scrolling(1.3);
			$(\'.gear-5\').scrolling(-0.4);
			$(\'.gear-1\').scrolling(-0.4);
			$(\'.gear-3\').scrolling(-0.9);
			$(\'.gear-2\').scrolling(-0.8);

			$(\'.gear-4\').arrowRotate(-0.8);
			$(\'.gear-5\').arrowRotate(-0.3);
			$(\'.gear-1\').arrowRotate(0.4);
			$(\'.gear-3\').arrowRotate(0.6);
			$(\'.gear-2\').arrowRotate(-1);

			$(\'.clock-arrow-1\').arrowRotate(6);
			$(\'.clock-arrow-2\').arrowRotate(0.5);
			}
		});
		}
	});
})(jQuery);',
		'ga_code' => '',
		'google_fonts_file' => '',
		'main_menu_font_family' => 'Montserrat',
		'main_menu_font_style' => 'regular',
		'main_menu_font_sets' => '',
		'main_menu_font_size' => '16',
		'main_menu_line_height' => '20',
		'submenu_font_family' => 'Montserrat',
		'submenu_font_style' => 'regular',
		'submenu_font_sets' => '',
		'submenu_font_size' => '16',
		'submenu_line_height' => '30',
		'styled_subtitle_font_family' => 'Dosis',
		'styled_subtitle_font_style' => '200',
		'styled_subtitle_font_sets' => '',
		'styled_subtitle_font_size' => '48',
		'styled_subtitle_line_height' => '57',
		'blog_post_title_font_family' => 'Dosis',
		'blog_post_title_font_style' => '200',
		'blog_post_title_font_sets' => '',
		'blog_post_title_font_size' => '48',
		'blog_post_title_line_height' => '57',
		'h1_font_family' => 'Montserrat',
		'h1_font_style' => '700',
		'h1_font_sets' => '',
		'h1_font_size' => '100',
		'h1_line_height' => '134',
		'h2_font_family' => 'Montserrat',
		'h2_font_style' => '700',
		'h2_font_sets' => '',
		'h2_font_size' => '70',
		'h2_line_height' => '134',
		'h3_font_family' => 'Montserrat',
		'h3_font_style' => '700',
		'h3_font_sets' => '',
		'h3_font_size' => '50',
		'h3_line_height' => '80',
		'h4_font_family' => 'Montserrat',
		'h4_font_style' => '700',
		'h4_font_sets' => '',
		'h4_font_size' => '40',
		'h4_line_height' => '41',
		'h5_font_family' => 'Montserrat',
		'h5_font_style' => '700',
		'h5_font_sets' => '',
		'h5_font_size' => '30',
		'h5_line_height' => '34',
		'h6_font_family' => 'Montserrat',
		'h6_font_style' => '700',
		'h6_font_sets' => '',
		'h6_font_size' => '20',
		'h6_line_height' => '28',
		'body_font_family' => 'Dosis',
		'body_font_style' => '300',
		'body_font_sets' => '',
		'body_font_size' => '21',
		'body_line_height' => '27',
		'widget_title_font_family' => 'Montserrat',
		'widget_title_font_style' => '700',
		'widget_title_font_sets' => '',
		'widget_title_font_size' => '30',
		'widget_title_line_height' => '40',
		'button_font_family' => 'Montserrat',
		'button_font_style' => 'regular',
		'button_font_sets' => '',
		'button_font_size' => '30',
		'button_line_height' => '52',
		'slideshow_title_font_family' => 'Roboto Condensed',
		'slideshow_title_font_style' => '300',
		'slideshow_title_font_sets' => '',
		'slideshow_title_font_size' => '80',
		'slideshow_title_line_height' => '100',
		'slideshow_description_font_family' => 'Roboto',
		'slideshow_description_font_style' => '100',
		'slideshow_description_font_sets' => '',
		'slideshow_description_font_size' => '36',
		'slideshow_description_line_height' => '50',
		'portfolio_title_font_family' => 'Dosis',
		'portfolio_title_font_style' => '300',
		'portfolio_title_font_sets' => '',
		'portfolio_title_font_size' => '21',
		'portfolio_title_line_height' => '30',
		'portfolio_description_font_family' => 'Dosis',
		'portfolio_description_font_style' => '300',
		'portfolio_description_font_sets' => '',
		'portfolio_description_font_size' => '19',
		'portfolio_description_line_height' => '27',
		'quickfinder_title_font_family' => 'Roboto Condensed',
		'quickfinder_title_font_style' => 'regular',
		'quickfinder_title_font_sets' => '',
		'quickfinder_title_font_size' => '21',
		'quickfinder_title_line_height' => '30',
		'quickfinder_description_font_family' => 'Source Sans Pro',
		'quickfinder_description_font_style' => '300',
		'quickfinder_description_font_sets' => '',
		'quickfinder_description_font_size' => '17',
		'quickfinder_description_line_height' => '24',
		'gallery_title_font_family' => 'Roboto Condensed',
		'gallery_title_font_style' => 'regular',
		'gallery_title_font_sets' => '',
		'gallery_title_font_size' => '21',
		'gallery_title_line_height' => '30',
		'gallery_description_font_family' => 'Source Sans Pro',
		'gallery_description_font_style' => '300',
		'gallery_description_font_sets' => '',
		'gallery_description_font_size' => '17',
		'gallery_description_line_height' => '24',
		'pricing_table_price_font_family' => 'Montserrat',
		'pricing_table_price_font_style' => '700',
		'pricing_table_price_font_sets' => '',
		'pricing_table_price_font_size' => '50',
		'pricing_table_price_line_height' => '56',
		'testimonial_font_family' => 'Montserrat',
		'testimonial_font_style' => '700',
		'testimonial_font_sets' => '',
		'testimonial_font_size' => '30',
		'testimonial_line_height' => '90',
		'counter_font_family' => 'Roboto',
		'counter_font_style' => '300',
		'counter_font_sets' => '',
		'counter_font_size' => '59',
		'counter_line_height' => '66',
		'woocommerce_price_font_family' => 'Roboto',
		'woocommerce_price_font_style' => '300',
		'woocommerce_price_font_sets' => '',
		'woocommerce_price_font_size' => '26',
		'woocommerce_price_line_height' => '36',
		'highlight_label_font_family' => 'Dosis',
		'highlight_label_font_style' => '300',
		'highlight_label_font_sets' => '',
		'highlight_label_font_size' => '21',
		'highlight_label_line_height' => '18',
		'basic_outer_background_color' => '#8b94a5',
		'basic_inner_background_color' => '#e8ecef',
		'top_background_color' => '#242c33',
		'main_background_color' => '#ffffff',
		'footer_background_color' => '',
		'styled_elements_background_color' => '#f1f5f8',
		'styled_elements_color_1' => '#ff7b61',
		'styled_elements_color_2' => '#ffb932',
		'divider_default_color' => '#d2dae1',
		'box_border_color' => '#d2dae1',
		'box_shadow_color' => '',
		'highlight_label_color' => '#ffb932',
		'main_menu_text_color' => '#ffffff',
		'main_menu_hover_text_color' => '#ffb932',
		'main_menu_active_text_color' => '#ffb932',
		'main_menu_active_background_color' => '',
		'submenu_text_color' => '#242c33',
		'submenu_hover_text_color' => '#58abb7',
		'submenu_background_color' => '#ffffff',
		'submenu_hover_background_color' => '#f1f5f8',
		'mega_menu_icons_color' => '#99a3b0',
		'menu_shadow_color' => '#4c5867',
		'body_color' => '#242c33',
		'h1_color' => '#242c33',
		'h2_color' => '#242c33',
		'h3_color' => '#242c33',
		'h4_color' => '#242c33',
		'h5_color' => '#242c33',
		'h6_color' => '#242c33',
		'link_color' => '#ff7b61',
		'hover_link_color' => '#0d2131',
		'active_link_color' => '#0d2131',
		'footer_text_color' => '#ffffff',
		'copyright_text_color' => '#ffffff',
		'copyright_link_color' => '#ffb932',
		'title_bar_background_color' => '#58ABB7',
		'title_bar_text_color' => '#ffffff',
		'top_area_text_color' => '#99a3b0',
		'date_filter_subtitle_color' => '#99a3b0',
		'system_icons_font' => '#99a3b0',
		'system_icons_font_2' => '#d2dae1',
		'button_text_basic_color' => '#242c33',
		'button_text_hover_color' => '#242c33',
		'button_text_active_color' => '#242c33',
		'button_background_basic_color' => '#ffb932',
		'button_background_hover_color' => '#ffb932',
		'button_background_active_color' => '#ffb932',
		'widget_title_color' => '',
		'widget_link_color' => '#418f9a',
		'widget_hover_link_color' => '#384554',
		'widget_active_link_color' => '#384554',
		'footer_widget_area_background_color' => '',
		'footer_widget_title_color' => '#ffb932',
		'footer_widget_text_color' => '#ffffff',
		'footer_widget_link_color' => '#58abb7',
		'footer_widget_hover_link_color' => '#ffb932',
		'footer_widget_active_link_color' => '#ffb932',
		'portfolio_slider_arrow_color' => '#58abb7',
		'portfolio_title_background_color' => '',
		'portfolio_title_color' => '#4c5867',
		'portfolio_description_color' => '#627080',
		'portfolio_sharing_button_background_color' => '#242c33',
		'portfolio_date_color' => '#99a3b0',
		'gallery_caption_background_color' => '',
		'gallery_title_color' => '',
		'gallery_description_color' => '',
		'slideshow_arrow_background' => '',
		'slideshow_arrow_color' => '#ff7070',
		'hover_effect_default_color' => '#161b21',
		'hover_effect_zooming_blur_color' => '#ffffff',
		'hover_effect_horizontal_sliding_color' => '#ff7070',
		'hover_effect_vertical_sliding_color' => '#384554',
		'quickfinder_bar_background_color' => '#ffffff',
		'quickfinder_bar_title_color' => '#4c5867',
		'quickfinder_bar_description_color' => '#384554',
		'quickfinder_title_color' => '#4c5867',
		'quickfinder_description_color' => '#384554',
		'bullets_symbol_color' => '#242c33',
		'icons_symbol_color' => '#58abb7',
		'pagination_border_color' => '',
		'pagination_text_color' => '#418f9a',
		'pagination_active_text_color' => '#ffffff',
		'pagination_active_background_color' => '#ffb932',
		'icons_slideshow_hovers_color' => '#ffffff',
		'mini_pagination_color' => '#d2dae1',
		'mini_pagination_active_color' => '#ff7070',
		'footer_social_color' => '#ffffff',
		'footer_social_hover_color' => '#ffb932',
		'top_area_icons_color' => '#99a3b0',
		'top_area_social_hover_color' => '#384554',
		'form_elements_background_color' => '#ffffff',
		'form_elements_text_color' => '#384554',
		'form_elements_border_color' => '#d2dae1',
		'basic_outer_background_image' => '',
		'basic_inner_background_image' => '',
		'top_background_image' => '',
		'main_background_image' => '',
		'footer_background_image' => '',
		'footer_widget_area_background_image' => get_stylesheet_directory_uri().'/images/footer-bg.jpg',
		'home_content_enabled' => '0',
		'home_content' => '',
		'slider_effect' => 'random',
		'slider_slices' => '15',
		'slider_boxCols' => '8',
		'slider_boxRows' => '4',
		'slider_animSpeed' => '5',
		'slider_pauseTime' => '20',
		'slider_directionNav' => '1',
		'slider_controlNav' => '1',
		'show_author' => '1',
		'excerpt_length' => '20',
		'footer_active' => '1',
		'footer_html' => '',
		'contacts_address' => '19th Ave New York, NY 95822, USA',
		'contacts_phone' => '+1 916-875-2235',
		'contacts_fax' => '+1 916-875-2235',
		'contacts_email' => 'info@domain.tld',
		'contacts_website' => 'www.codex-themes.com',
		'admin_email' => '',
		'twitter_active' => '1',
		'facebook_active' => '1',
		'linkedin_active' => '1',
		'googleplus_active' => '1',
		'stumbleupon_active' => '1',
		'rss_active' => '1',
		'twitter_link' => '#',
		'facebook_link' => '#',
		'linkedin_link' => '#',
		'googleplus_link' => '#',
		'stumbleupon_link' => '#',
		'rss_link' => '#',
		'show_social_icons' => '1'
	);
}
add_filter('scalia_default_theme_options', 'scalia_scrollex_first_install_settings');