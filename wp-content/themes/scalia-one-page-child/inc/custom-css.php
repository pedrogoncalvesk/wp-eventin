#primary-menu.no-responsive > li.menu-item-current:hover > a,
#primary-menu.no-responsive > li.menu-item-active:hover > a {
	color: <?php echo scalia_get_option('main_menu_hover_text_color'); ?>;
}

<?php if(scalia_get_option('body_color')) : ?>
.widget_categories > ul li > a,
.widget_product_categories > ul li > a,
.widget_pages > ul li > a {
	color: <?php echo scalia_get_option('body_color'); ?>;
}
<?php endif; ?>